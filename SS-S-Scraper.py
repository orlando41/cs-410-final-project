#!/usr/bin/env python
# coding: utf-8

#Scraping Food Pages

#imports
from bs4 import BeautifulSoup
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options
import re 
import urllib
import time

#create a webdriver object and set options for headless browsing
options = Options()
options.headless = True
options.add_experimental_option('excludeSwitches', ['enable-logging'])
options.add_argument("--incognito")
options.add_argument("--disable-extensions")
options.add_argument("test-type")
#driver = webdriver.Chrome(executable_path='<path-to-chrome>', options=options)
browser = webdriver.Chrome('./chromedriver',options=options)

#uses webdriver object to execute javascript code and get dynamically loaded webcontent
def get_js_soup(url,browser):
    browser = webdriver.Chrome()
    browser.get(url)
    browser.refresh()

    res_html = browser.execute_script('return document.body.innerHTML')
    soup = BeautifulSoup(res_html,'html.parser') #beautiful soup object to be used for parsing html content

    return soup

''' More tidying
Sometimes the text extracted HTML webpage may contain javascript code and some style elements. 
This function removes script and style tags from HTML so that extracted text does not contain them.
'''
def remove_script(soup):
    for script in soup(["script", "style"]):
        script.decompose()
    return soup

#extracts all Sandwich page urls from the Menu
def scrape_menu_page(dir_url,browser):
    
    print('-'*20,'Scraping restaurant page','-'*20)
    
    #empty list to hold sandwich links
    item_links = []
    
    #empty list to hold sandwich names
    item_names_list = []
    
    #this is only for Subway at 2392
    item_base_url = 'https://order.subway.com'
    
    #execute js on webpage to load sandwich listings on webpage and get ready to parse the loaded HTML 
    soup = get_js_soup(dir_url,browser)
    
    #grabbing section of html code with sandwich names/links
    menu = soup.find(class_= "product-grid product-list")
    
    #grabbing sandwich names first
    item_names = menu.find_all(class_ = "category-title")
    for item in item_names:
        name = item.contents[0]
        name = name.strip('\n')
        item_names_list.append(name.strip())

    #grabbing sandwich links
    for item in menu.find_all('a'):
        link = item.get('href')
        #url returned is relative, so we need to add base url
        item_links.append(item_base_url+link) 
    print ('-'*20,'Found {} Subway Sandwich profile urls'.format(len(item_links)),'-'*20)
    
    return item_links, item_names_list

#main part of url carried through will all sandwiches, only looking at one local store
s_url = 'https://order.subway.com/en-US/restaurant/2392/menu/category/3' #url of directory listings of CS faculty

#empty list to hold sandwich links
sand_links = []

#putting a delay between lookup requests
sleepy_time = 1

#there are problems with the beautiful soup returning null values
##need to make sure no null values are returned
while len(sand_links) == 0:
    try:
        sand_links,sand_names = scrape_menu_page(s_url,browser)
    except:
        print('menu scraping did not work :(')
        time.sleep(sleepy_time)
        sleepy_time+=1


def scrape_sandwich_page(s_url,browser):

    soup = get_js_soup(s_url,browser)
    
    s_page = soup.find(id = 'price')
    
    price = s_page.get_text()

    return price

sand_prices = []
        
#Scrape all sandwich homepages using profile page urls to find prices
for link in sand_links:
    sleepy_time = 1
    s_price = scrape_sandwich_page(link,browser)
    while s_price == '':
            s_price = scrape_sandwich_page(link,browser)  
            time.sleep(sleepy_time)
            sleepy_time+=1
    sand_prices.append(s_price)

# Finally, write urls and extracted bio to txt files
def write_lst(sandwiches,prices,file_):
    with open(file_,'w') as f:
        for sw,money in zip(sandwiches,prices):
            sw = sw.replace(',','')
            f.write(sw)
            f.write(',')
            f.write(money)
            f.write('\n')

sandwich_file = 'Subway-S.txt'
write_lst(sand_names,sand_prices,sandwich_file)