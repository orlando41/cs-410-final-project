#!/usr/bin/env python
# coding: utf-8

#Scraping Food Pages

#imports
from bs4 import BeautifulSoup
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import re 
import urllib
import html5lib

#create a webdriver object and set options for headless browsing
options = Options()
options.headless = True
options.add_experimental_option("detach", True)
options.add_experimental_option('excludeSwitches', ['enable-logging'])
#driver = webdriver.Chrome(executable_path='<path-to-chrome>', options=options)
browser = webdriver.Chrome('./chromedriver',options=options)

#uses webdriver object to execute javascript code and get dynamically loaded webcontent
def get_js_soup(url,browser):
    browser = webdriver.Chrome()
    browser.get(url)

    res_html = browser.execute_script('return document.body.innerHTML')

    soup = BeautifulSoup(res_html,'lxml') #beautiful soup object to be used for parsing html content

    return soup

#extracts all Sandwich page urls from the Menu
def scrape_menu_page(dir_url,browser):
    
    #empty list to hold sandwich names
    sandwich_names_list = []

    #empty list to hold sandwich prices
    prices_list = []

    #execute js on webpage to load sandwich listings on webpage and get ready to parse the loaded HTML 
    soup = get_js_soup(dir_url,browser)
    
    menu = soup.find(id = "group_35172")
    #grabbing sandwich names first
    sandwich_names = menu.find_all(class_ = "item-name")
    for sandwich in sandwich_names:
        name = sandwich.contents[0]
        name = name.strip('\n')
        sandwich_names_list.append(name.strip())
    
    #grabbing sandwich prices
    pricing = menu.find_all(class_ = "item-price")
    for sandwich in pricing:
        s_price = sandwich.contents[0]
        s_price = s_price.strip('\n')
        prices_list.append(s_price.strip())
    
    return sandwich_names_list,prices_list

#main part of url carried through will all sandwiches, only looking at one local store
dir_url = 'https://www.snapfinger.com/o/c/McAlisters/restaurant/26931/group/35172' #url of directory listings of CS faculty

sandwich_names,prices = scrape_menu_page(dir_url,browser)

# Finally, write urls and extracted bio to txt files
def write_lst(sandwiches,prices,file_):
    with open(file_,'w') as f:
        for sw,money in zip(sandwiches,prices):
            sw = sw.replace(',','')
            f.write(sw)
            f.write(',')
            f.write(money)
            f.write('\n')

sandwich_file = 'McAlisters.txt'
write_lst(sandwich_names,prices,sandwich_file)