#!/usr/bin/env python
# coding: utf-8

# In[67]:


#All the things we need to import
import matplotlib.pyplot as plt
import numpy as np


# In[74]:


#function to read in files and make data sets
def read_write (filename):
    sandwiches = []
    prices = []
    f = open(filename, 'r')
    f1 = f.readlines()
    for line in f1:
        sline = line.split(',')
        sandwiches.append(sline[0])
        tprice = sline[1]
        tprice = tprice.replace('\n','')
        tprice = tprice.replace('$','')
        prices.append(float(tprice))
    return prices,sandwiches

#makes giant list of all the values for the graph
def makinglists(SSnames,SSprices,SScolor,SSres,SWnames,SWprices,SWcolor,SWres,HWnames,HWprices,HWcolor,HWres,MCnames,MCprices,MCcolor,MCres):
    SS = []
    SS.append(SSnames)
    SS.append(SSprices)
    SS.append(SScolor)
    SS.append(SSres)

    SW = []
    SW.append(SWnames)
    SW.append(SWprices)
    SW.append(SWcolor)
    SW.append(SWres)

    HW = []
    HW.append(HWnames)
    HW.append(HWprices)
    HW.append(HWcolor)
    HW.append(HWres)

    MC = []
    MC.append(MCnames)
    MC.append(MCprices)
    MC.append(MCcolor)
    MC.append(MCres)
    
    return(SS,SW,HW,MC)

#return prices and names of a type of sandwich, turkey/steak/chicken
def finding_words(text,names,prices):
    indexer = 0
    to_cut = []
    while indexer < len(names):
        if text not in names[indexer]:
            to_cut.append(indexer)
        indexer+=1
    indexer = len(to_cut) - 1
    while indexer >= 0:
        names.pop(to_cut[indexer])
        prices.pop(to_cut[indexer])
        indexer-=1
    return names,prices

#function to graph
def plotter(lists,graphtitle,filename):
    fig, ax = plt.subplots()
    for restaurant in lists:
        x = restaurant[0]
        y = restaurant[1]
        c = restaurant[2]
        c = c[0]
        l = restaurant[3]
        l = l[0]

        ax.scatter(x,y,c = c, label = l)

    ax.legend()

    cur_axes = plt.gca()
    cur_axes.axes.get_xaxis().set_ticks([])
    #cur_axes.axes.get_xaxis().set_visible(False)
    plt.xlabel('Menu Item')
    plt.ylabel('Price ($)')
    plt.title(graphtitle)
    plt.savefig(filename)
    plt.show()
    return

#loading in Subway Sandwiches
SS_prices,SS_names = read_write('Subway-S.txt')

#loading in Subway Wraps
SW_prices,SW_names = read_write('Subway-W.txt')

#loading in Headwest Sandwiches
HW_prices,HW_names = read_write('Head_West.txt')

#loading in Mcalister's Sandwiches/Wraps
MC_prices,MC_names = read_write('McAlisters.txt')

#designate colors for each restaurant 
SS_color = ['g']
SW_color = ['y']
HW_color = ['b']
MC_color = ['r']

SS_res = ['Subway Sandwiches']
SW_res = ['Subway Wraps']
HW_res = ['Headwest']
MC_res = ['McAlisters']


# In[75]:


#########plotting all restaurants and their prices #################
SS,SW,HW,MC = makinglists(SS_names,SS_prices,SS_color,SS_res,SW_names,SW_prices,SW_color,SW_res,HW_names,HW_prices,HW_color,HW_res,MC_names,MC_prices,MC_color,MC_res)

plotter([SS,SW,HW,MC],'Restaurants and Price Groupings','RvsP.pdf')


# In[76]:



#########plotting all Turkey Sandwiches and their prices #################
text = 'Turkey'
SST_names,SST_prices = finding_words(text,SS_names,SS_prices)
SWT_names,SWT_prices = finding_words(text,SW_names,SW_prices)
HWT_names,HWT_prices = finding_words(text,HW_names,HW_prices)
MCT_names,MCT_prices = finding_words(text,MC_names,MC_prices)

SST,SWT,HWT,MCT = makinglists(SST_names,SST_prices,SS_color,SS_res,SWT_names,SWT_prices,SW_color,SW_res,HWT_names,HWT_prices,HW_color,HW_res,MCT_names,MCT_prices,MC_color,MC_res)

plotter([SST,SWT,HWT,MCT],'Turkey Comparisons','Turkey.pdf')


# In[65]:


#loading in Subway Sandwiches
SS_prices,SS_names = read_write('Subway-S.txt')

#loading in Subway Wraps
SW_prices,SW_names = read_write('Subway-W.txt')

#loading in Headwest Sandwiches
HW_prices,HW_names = read_write('Head_West.txt')

#loading in Mcalister's Sandwiches/Wraps
MC_prices,MC_names = read_write('McAlisters.txt')

#########plotting all Chicken Sandwiches and their prices #################
text = 'Chicken'
SSC_names,SSC_prices = finding_words(text,SS_names,SS_prices)
SWC_names,SWC_prices = finding_words(text,SW_names,SW_prices)
HWC_names,HWC_prices = finding_words(text,HW_names,HW_prices)
MCC_names,MCC_prices = finding_words(text,MC_names,MC_prices)

SSC,SWC,HWC,MCC = makinglists(SSC_names,SSC_prices,SS_color,SS_res,SWC_names,SWC_prices,SW_color,SW_res,HWC_names,HWC_prices,HW_color,HW_res,MCC_names,MCC_prices,MC_color,MC_res)

plotter([SSC,SWC,HWC,MCC],'Chicken Comparisons','Chicken.pdf')


# In[77]:


#loading in Subway Sandwiches
SS_prices,SS_names = read_write('Subway-S.txt')

#loading in Subway Wraps
SW_prices,SW_names = read_write('Subway-W.txt')

#loading in Headwest Sandwiches
HW_prices,HW_names = read_write('Head_West.txt')

#loading in Mcalister's Sandwiches/Wraps
MC_prices,MC_names = read_write('McAlisters.txt')

#########plotting all Veggie Sandwiches and their prices #################
text = 'Veggie'
SSV_names,SSV_prices = finding_words(text,SS_names,SS_prices)
SWV_names,SWV_prices = finding_words(text,SW_names,SW_prices)
HWV_names,HWV_prices = finding_words(text,HW_names,HW_prices)
MCV_names,MCV_prices = finding_words(text,MC_names,MC_prices)

SSV,SWV,HWV,MCV = makinglists(SSV_names,SSV_prices,SS_color,SS_res,SWV_names,SWV_prices,SW_color,SW_res,HWV_names,HWV_prices,HW_color,HW_res,MCV_names,MCV_prices,MC_color,MC_res)

plotter([SSV,SWV,HWV,MCV],'Veggie Comparisons','Veggie.pdf')


# In[98]:


##########create a value per inch graph#########################
#loading in Subway Sandwiches
SS_prices,SS_names = read_write('Subway-S.txt')

#loading in Headwest Sandwiches
HW_prices,HW_names = read_write('Head_West.txt')

subway_length = 12
hw_length = 8

SSL_prices = []
for sprice in SS_prices:
    sprice = sprice / subway_length
    SSL_prices.append(round(sprice,2))


HWL_prices = []
for hprice in HW_prices:
    hprice = hprice / hw_length
    HWL_prices.append(round(hprice,2))
    
SS = []
SS.append(SS_names)
SS.append(SSL_prices)
SS.append(SS_color)
SS.append(SS_res)

HW = []
HW.append(HW_names)
HW.append(HWL_prices)
HW.append(HW_color)
HW.append(HW_res)

plotter([SS,HW],'Price Per Inch','PPI.pdf')


# In[ ]:




