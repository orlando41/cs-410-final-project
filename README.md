# CS-410-Final-Project
**This code is run using Python 3.7.4

**This code requires Chromedriver and needs to be in the same folder when code is run.

Intro:
* This code scrapes multiple restaurant (Subway Sandwich, Headwest Sub Shop, and McAlisters) sandwich and wrap pages.
* It outputs text files of item names and prices in a text file. 
* The program Plotter.py will produce a number of graphs.

Instructions to Run:
* Run ScriptRunner.sh in terminal (this is one command that will run all the restaurant python programs).
* Look at text output files or run Plotter.py to see graphs.

---------------------------------------------Breakdown of Programs---------------------------------------------

Plotter.py
* Takes in output text files and create graphs used for powerpoint presentation.
* presentation website: https://drive.google.com/file/d/1ytxfmKCsPuk-yMhPlIr2Asm27mifVWLw/view

SS-S-Scraper.py
* Scrapes the sandwich portion of the menu of a Subway Sandwich shop.
* Grabs the sandwich names and prices (prices in terms of a footlong/12”)
* I split the sandwiches and wraps into two different programs because there were some problems with beautiful soup randomly retrieving null values so I wanted to tackle them one at a time.  It would be simple to combine them into one program but for my purposes it was easier to keep them separate.
* website: https://order.subway.com/en-US/restaurant/2392/menu/category/3

SS-W-Scraper.py
* Scrapes the wrap portion of the menu of a Subway Sandwich shop.
* Grabs wrap names and prices (only one size of wrap, typically 8-10”) 
* There were also some problems with beautiful soup randomly retrieving null values.
* website: https://order.subway.com/en-US/restaurant/2392/menu/category/17

HW-Scraper.py
* Scrapes the sandwich portion of the menu of a local restaurant names Headwest Sub Shop.
* This is seen as “the best” as it has been voted “best sandwich” for at least 15 years in a row.
* Grabs sandwich names and prices (prices in terms of a full sandwich about 8”)
* This page was very straightforward in that prices were listed on the website instead of an online order page.
* website: https://www.headwestsubs.com/menu

MC-Scraper.py
* Scrapes the sandwich portion of the menu of a McAlister’s Deli.
* Grabs the names of both sandwiches and wraps together as well as their prices.
* Wrap and sandwich sizes were not explicitly specified and I could not find any extra information regarding them. 
* website: https://www.snapfinger.com/o/c/McAlisters/Restaurant/26931?_ga=2.209945366.2061387869.1576103319-44895186.1576103319

JJ-Scraper.py (not functional)
* Scrapes the sandwich portion of the menu at a Jimmy John’s.
* Grabs sandwich names and prices.
* This website includes some functionality where the user has to click in order to see the hidden prices and I was not able to figure out this functionality with Chromedriver due to the time constraint.
* website: https://online.jimmyjohns.com/menu/jj0135