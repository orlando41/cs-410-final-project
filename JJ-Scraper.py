#!/usr/bin/env python
# coding: utf-8

#Scraping Food Pages

#imports
from bs4 import BeautifulSoup
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import re 
import urllib

#create a webdriver object and set options for headless browsing
options = Options()
options.headless = True
options.add_experimental_option("detach", True)
options.add_experimental_option('excludeSwitches', ['enable-logging'])
#driver = webdriver.Chrome(executable_path='<path-to-chrome>', options=options)
browser = webdriver.Chrome('./chromedriver',options=options)

#uses webdriver object to execute javascript code and get dynamically loaded webcontent
def get_js_soup(url,browser):
    browser = webdriver.Chrome()
    browser.get(url)
    #counter = 1
    #while counter !=0:
    #    counter+=1
    #NEXT_BUTTON_XPATH = '//*[@id="ember605"]/div[2]/div[1]/div[1]/div[1]/button'
    #button = browser.find_element_by_xpath(NEXT_BUTTON_XPATH)
    #button.click()
    
    #browser.refresh()
    #python_button = browser.find_elements_by_xpath("//*[contains(text(), 'Favorites')]")
    #print(python_button)
    #python_button.click()
    #browser.refresh()
    res_html = browser.execute_script('return document.body.innerHTML')

    soup = BeautifulSoup(res_html,'html.parser') #beautiful soup object to be used for parsing html content

    return soup

#tidies extracted text 
def process_bio(bio):
    bio = bio.encode('ascii',errors='ignore').decode('utf-8')       #removes non-ascii characters
    bio = re.sub('\s+',' ',bio)       #repalces repeated whitespace characters with single space
    return bio

''' More tidying
Sometimes the text extracted HTML webpage may contain javascript code and some style elements. 
This function removes script and style tags from HTML so that extracted text does not contain them.
'''
def remove_script(soup):
    for script in soup(["script", "style"]):
        script.decompose()
    return soup

####Check to see if I need this, probably needs to be changed
#Checks if bio_url is a valid faculty homepage
def is_valid_homepage(bio_url,dir_url):
    try:
        #sometimes the homepage url points to the faculty profile page
        #which should be treated differently from an actual homepage
        ret_url = urllib.request.urlopen(bio_url).geturl() 
    except:
        return False       #unable to access bio_url
    urls = [re.sub('((https?://)|(www.))','',url) for url in [ret_url,dir_url]] #removes url scheme (https,http) or www 
    return not(urls[0]== urls[1])

#extracts all Sandwich page urls from the Menu
def scrape_menu_page(dir_url,browser):
    
    print('-'*20,'Scraping restaurant page','-'*20)
    
    #empty list to hold sandwich links
    sandwich_links = []
    
    #empty list to hold sandwich names
    sandwich_names_list = []
    
    #this is only for Subway at 2392
    sandwich_base_url = 'https://online.jimmyjohns.com/menu/jj0135'
    
    #execute js on webpage to load sandwich listings on webpage and get ready to parse the loaded HTML 
    soup = get_js_soup(dir_url,browser)
    #soup = remove_script(soup)
    print(soup.prettify())
    #grabbing section of html code with sandwich names/links
    menu = soup.find_all(class_ = '_disclosureSection_1jnlhy')
    #print(menu)
    '''
    #grabbing sandwich names first
    sandwich_names = menu.find_all(class_ = "category-title")
    for sandwich in sandwich_names:
        name = sandwich.contents[0]
        name = name.strip('\n')
        sandwich_names_list.append(name.strip())

    #grabbing sandwich links
    for item in menu.find_all('a'):
        link = item.get('href')
        #url returned is relative, so we need to add base url
        sandwich_links.append(sandwich_base_url+link) 
    print ('-'*20,'Found {} Subway Sandwich profile urls'.format(len(sandwich_links)),'-'*20)
    '''
    return sandwich_links, sandwich_names_list

#main part of url carried through will all sandwiches, only looking at one local store
dir_url = 'https://online.jimmyjohns.com/menu/jj0135' #url of directory listings of CS faculty

sandwich_links,sandwich_names = scrape_menu_page(dir_url,browser)


def scrape_sandwich_page(s_url,browser):

    soup = get_js_soup(s_url,browser)
    
    s_page = soup.find(id = 'price')
    
    price = s_page.get_text()

    return price
'''
prices = []

#Scrape all sandwich homepages using profile page urls to find prices
for link in sandwich_links:
    s_price = scrape_sandwich_page(link,browser)
    if s_price == '':
        s_price = scrape_sandwich_page(link,browser)
        prices.append(s_price)
    else:
        prices.append(s_price)
        
#do a final check and make sure no values are missing
enumerator = 0
while enumerator < len(sandwich_links):
    if prices[enumerator] == '':
        temp_price = scrape_sandwich_page(sandwich_links[enumerator],browser)
        prices[enumerator] = temp_price
    enumerator +=1

print(prices)

# Finally, write urls and extracted bio to txt files
def write_lst(sandwiches,prices,file_):
    with open(file_,'w') as f:
        for sw,money in zip(sandwiches,prices):
            f.write(sw)
            f.write(',')
            f.write(money)
            f.write('\n')

sandwich_file = 'Jimmy_Johns.txt'
write_lst(sandwich_names,prices,sandwich_file)
'''

